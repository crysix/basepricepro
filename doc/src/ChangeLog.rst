.. |date| date:: %d.%m.%Y

.. |year| date:: %Y

.. footer::
   .. class:: footertable
   
   +-------------------------+-------------------------+
   | Stand: |date|           | .. class:: rightalign   |
   |                         |                         |
   |                         | ###Page###/###Total###  |
   +-------------------------+-------------------------+


.. sectnum::

==============================
Grundpreis Modul (Pro-Upgrade)
==============================


ChangeLog
=========

.. list-table::
   :header-rows: 1
   :widths: 1 1 6

   * - **Revision**
     - **Datum**
     - **Beschreibung**

  

   * - 13.01.25
     - 25.01.2013
     - Bugfixes:

       * Übersetzungskonflikt mit Mage_Customer
       * Bug bei der Preisaktualisierung mit Custom Options

   * - 0.2.2
     - 04.05.2012
     - Initiale Version
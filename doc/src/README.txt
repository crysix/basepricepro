/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BasePricePro
 * @copyright  Copyright (c) 2011 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @copyright  Copyright (c) 2012 Netresearch GmbH 
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html  (DMCSL 1.0)
 */

ABOUT
This extension extends the Magento community module BasePrice and adds support for
custom options, configurable products and bundle products.

Install this module in addition to the free BasePrice extension, NOT instead of it.

For information about how to configure the extension please see the BasePrice
community extension.


CHANGELOG
0.2.1 Fix base price calculation when special price is set
0.2.0 Move frontend theme files to base interface
0.1.9 Minor BCP compatibility update
0.1.8 Magento 1.4 compatibility update, BCP compatibility update
0.1.7 Bugfix regarding IE8
0.1.6 JavaScript changes to make this extension compatible with the upcoming Better Configurable Product extension (BCP)
0.1.5 ??? 
0.1.4 Added product base_price_amount to simple product quick create form for associated products
0.1.3 Initial Release


BUGS
If you have ideas for improvements or find bugs, please send them to vinai@der-modulprogrammierer.de,
with DerModPro_BasePricePro as part of the subject.
